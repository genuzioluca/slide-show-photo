<?php

require '.vendor/autoload.php';

use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;

$request = Request::createFromGlobals();

if ($request->get('next') !== null) {
    $session = new Session(new NativeSessionStorage([
        'name' => 'SlideShowSId',
    ]));
    $finder = new Finder();

    $path = (string) $request->get('path', '');
    $inPath = __DIR__.('' !== __DIR__ && $path !== '' ? '/' : '').$path;
    $outPath = $path.($path !== '' ? '/' : '');

    $finder
        ->name('*.{jpg,JPG,jpeg,JPEG}')
        ->sortByName()
        ->files()
        ->followLinks()
        ->exclude('.vendor')
        ->in($inPath);

    // Immagini presenti in Data
    $immaginiDaMostrate = [];
    foreach ($finder as $file) {
        $immaginiDaMostrate[] = $outPath.$file->getRelativePathname();
    }

    $sessionKey = 'img_show_'.md5($path);

    // Immagini già mostrate
    $immaginiMostrate = $session->get($sessionKey, []);

    // Solo le immagini ancora non visualizzate
    $immagini = array_diff($immaginiDaMostrate, $immaginiMostrate);

    // La prima immagine
    $immagine = reset($immagini);

    if ($immagine === false) {
        $immagine = reset($immaginiDaMostrate);
        $immaginiMostrate = [];
    }

    // Aggiungo l'immagine mostrata
    $immaginiMostrate[] = $immagine;

    $session->set($sessionKey, $immaginiMostrate);

    $response = RedirectResponse::create($immagine);
} else {
    $time = (int) $request->get('time', 10);
    if ($time < 1) {
        $time = 10;
    }

    $response = new Response(<<<EOF
<html>
<head>
    <style>
    html, body {
        height: 100%;
        margin: 0;
        padding: 0;
        background: black;
    }

        img {
        padding: 0;
        display: block;
        margin: 0 auto;
            height: 100%;
            width: 100%;
            object-fit: contain;
        }
    </style>  
</head>
<body>
<img class="center-fit" id="img">
<script type="text/javascript">
    "use strict";
    function update(){         
        document.getElementById('img').src = 'index.php?next=' + new Date().getTime() + '&path={$request->get('path', '')}';    
    }
    update();
    setInterval(update, 1000 * $time);
</script>
</body>
</html>
EOF
    );
}

$response->send();
